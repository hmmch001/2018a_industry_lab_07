package ictgradschool.industry.lab07.ex02;

import ictgradschool.Keyboard;

/**
 * A guessing game!
 */
public class GuessingGame {

    /**
     * Plays the actual guessing game.
     *
     * You shouldn't need to edit this method for this exercise.
     */
    public void start() {

        int number = getRandomValue();
        int guess = 0;

        while (guess != number) {

            guess = getUserGuess();

            if (guess == -1){
                System.out.println("Please re-enter a valid guess:");
            }else if(guess > number){
                System.out.println("Too high!");
            }
            else if (guess < number) {
                System.out.println("Too low!");
            }
            else {
                System.out.println("Perfect!");
            }

        }

    }

    /**
     * Gets a random integer between 1 and 100.
     *
     * You shouldn't need to edit this method for this exercise.
     */
    private int getRandomValue() {
        return (int) (Math.random() * 100) + 1;
    }


    private int getUserGuess() {
        System.out.print("Enter your guess: ");
        int guess;
        try {
            guess = Integer.parseInt(Keyboard.readInput());
            if (guess < 1 || guess > 100) {
                throw new IllegalArgumentException("Value not between 1-100.");
            }

        }catch (NumberFormatException e ) {
            System. out .println( "Value is not a number." );
            return -1;
        }catch(IllegalArgumentException e){
            System.out.println(e);
            return -1;
        }
        return guess;
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        GuessingGame ex = new GuessingGame();
        ex.start();

    }
}
