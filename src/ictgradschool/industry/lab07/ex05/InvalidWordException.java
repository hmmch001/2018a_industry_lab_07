package ictgradschool.industry.lab07.ex05;

public class InvalidWordException extends Exception {
    public InvalidWordException() { }

    public String toString() {
        return "Input contains invalid words.";
    }
}
