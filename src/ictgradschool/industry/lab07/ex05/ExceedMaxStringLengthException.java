package ictgradschool.industry.lab07.ex05;

public class ExceedMaxStringLengthException extends Exception {
    public ExceedMaxStringLengthException() { }

    public String toString() {
        return "Input too long.";
    }
}
