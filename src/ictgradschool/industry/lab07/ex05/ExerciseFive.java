package ictgradschool.industry.lab07.ex05;

/**
 * TODO Write a program according to the Exercise Five guidelines in your lab handout.
 */
public class ExerciseFive {

    public void start() {
        String firstLetters;
        String userInput = "";
        boolean tooLong = false;

        try {
            userInput = getStringFromUser();
        } catch (ExceedMaxStringLengthException e ){
            System.out.println(e);
            tooLong = true;
        }
        if(!tooLong) {
            try {
                firstLetters = getFirstLetters(userInput);
                System.out.println("You entered: " + firstLetters);
            } catch (InvalidWordException e) {
                System.out.println(e);
            }
        }



    }

    private String getStringFromUser() throws ExceedMaxStringLengthException {

        System.out.print("Enter a string of at most 100 characters: ");
        String userInput = Keyboard.readInput();
        if( userInput.length() > 100){
            throw new ExceedMaxStringLengthException();
        }
        return userInput;


    }
    private String getFirstLetters(String userInput) throws InvalidWordException{
        String firstLetters="";
        if(Character.isLetter(userInput.charAt(0))){
            firstLetters = firstLetters + userInput.charAt(0);
        }
        for(int i = 1; i < userInput.length(); i ++) {
            if (userInput.charAt(i) == ' ') {
                if (!Character.isLetter(userInput.charAt(i + 1))) {
                    throw new InvalidWordException();
                } else {
                    firstLetters = firstLetters + " " + userInput.charAt(i + 1);
                }

            }
        }

        return firstLetters;
    }



    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {
        new ExerciseFive().start();
    }
}
