package ictgradschool.industry.lab07.ex04;

public class IndexTooHighException  extends Exception {
    public IndexTooHighException() { }

    public String toString() {
        return "Index too high. Please try again.";
    }
}
