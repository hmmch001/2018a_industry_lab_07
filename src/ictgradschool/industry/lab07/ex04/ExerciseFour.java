package ictgradschool.industry.lab07.ex04;

import ictgradschool.Keyboard;

/**
 * A simple program that generates an array of random numbers, then displays
 * one of them (user's choice).
 */
public class ExerciseFour {


    /**
     * Runs the program.
     *
     * TODO Handle your InvalidIndexException, IndexTooLowException, and IndexTooHighException appropriately.
     */
    public void start() {


        int[] myArray = generateArray();
        int index = 0;
        boolean invalidInput = true;
        while(invalidInput){

                try {
                    index = getArrayIndexFromUser();
                    invalidInput = false;
                } catch (InvalidIndexException e){
                    System.out.println(e.toString());
                } catch (IndexTooLowException e){
                    System.out.println(e.toString());
                } catch(IndexTooHighException e){
                    System.out.println(e.toString());
                }

        }

        System.out.println("The element at index " + index + " is: " + myArray[index]);
    }



      private int getArrayIndexFromUser() throws IndexTooLowException, IndexTooHighException, InvalidIndexException{

        System.out.print("Enter an index: ");
          int index;
        try {
            index = Integer.parseInt(Keyboard.readInput());

            if (index < 0) {
                throw new IndexTooLowException();
            }else if (index > 4){
                throw new IndexTooHighException();
            }
        }catch(NumberFormatException e){
            throw new InvalidIndexException();
        }

        return index;

    }

    /**
     * Creates and returns an array with five random numbers.
     */
    private int[] generateArray() {
        int [] randomArray = new int[5];
        for(int i = 0; i < randomArray.length; i++){
            randomArray[i] = (int)((Math.random()*1000)+1);
        }
        return randomArray;
    }

    public static void main(String[] args) {
        new ExerciseFour().start();
    }

}
