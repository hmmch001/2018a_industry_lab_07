package ictgradschool.industry.lab07.ex04;

public class InvalidIndexException extends Exception{

    public InvalidIndexException() { }

    public String toString() {
        return "Invalid index.  Please try again.";
    }

}
